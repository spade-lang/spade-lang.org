+++
title = "Spade at LatchUp 2024"
weight = 1
+++

*Links*
- [Spade website](https://spade-lang.org)
- [Spade documentation](https://docs.spade-lang.org)
- [Slides](../latchup24.pdf)

*Contact and Socials*

- [mastodon.social/@thezoq2](https://mastodon.social/@thezoq2)
- [frans.skarman@liu.se](mailto:frans.skarman@liu.se)
- [Linkedin](https://www.linkedin.com/in/frans-skarman-20b781171/)

