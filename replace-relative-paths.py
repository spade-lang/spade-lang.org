"""
Replace absolute links with relative links. This makes the generated HTML not
depend on *where* it is located relative to the domain root. Very useful for the
preview job in the CI!

Limitations:

- Only considers .html-files.
- Only works correctly if run exactly once since it replaces /a/b with ../a/b
  which will be matched again if the script is re-run.
"""

from io import SEEK_SET
from pathlib import Path


def replace_in_file(path, fr, to):
    """Replace all occurences of `fr` with `to` in file `path`.

    Do not write the file if there is nothing to replace.
    """
    with open(path, mode="r+") as f:
        content: str = f.read()
        if fr in content:
            print(f"replacing {fr} in {path}")
            # clear the file first
            f.seek(0, SEEK_SET)
            f.truncate()
            # then write the replaced content
            f.write(content.replace(fr, to))


def main():
    # we want to replace links to static files since zola should figure out page links
    static_files = [d / f for d, _, fnames in Path("static").walk() for f in fnames]
    static_files = [
        Path.joinpath(*list(map(Path, (p.parts[1:])))) for p in static_files
    ]
    print(f"{static_files=}")

    for p, _, fnames in Path("public").walk():
        # how many '../' to add
        depth = len(p.parents) - 1
        for f in fnames:
            if Path(f).suffix == ".html":
                fullpath = p / f
                print(depth, fullpath)
                for s in static_files:
                    replacement = ("./" if depth == 0 else ("../" * depth)) + f"{s}"
                    replace_in_file(fullpath, f"/{s}", replacement)
                    # html escape too because why would stuff be easy
                    replace_in_file(fullpath, f"&#x2F;{s}", replacement)


main()
